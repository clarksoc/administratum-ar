import 'dart:core';

import 'package:administratum_ar/models/unit_db_details.dart';
import 'package:flutter/material.dart';

class UnitModel {
  final UnitClass unit;
  final FactionClass faction;
  final List<WeaponClass> weapons;
  final String glbRef;
  final bool isCustom;
  final String bundleUrl;
  final String modelName;

  UnitModel({
    @required this.unit,
    @required this.faction,
    @required this.weapons,
    @required this.glbRef,
    this.isCustom = false,
    this.bundleUrl = "",
    this.modelName = "",
  });
}

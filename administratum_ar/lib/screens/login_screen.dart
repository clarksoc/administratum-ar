import 'package:administratum_ar/screens/main_menu_screen.dart';
import 'package:administratum_ar/widgets/loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  final String title;

  LoginScreen({Key key, this.title}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences preferences;

  bool isLoading = false;
  bool isLogIn = false;

  User currentUser;

  Future<void> _handleSignIn() async {
    try {
      await googleSignInHandler();
    } catch (error) {
      print(error);
    }
  }

  Future<Null> googleSignInHandler() async {
    preferences = await SharedPreferences.getInstance();
    this.setState(() {
      isLoading = true;
    });

    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    if (googleSignInAccount == null) {
      setState(() {
        isLoading = false;
      });
      print("USER CANCELLED SIGN IN");
      return null;
    }

    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    AuthCredential credential = GoogleAuthProvider.credential(
      idToken: googleSignInAuthentication.idToken,
      accessToken: googleSignInAuthentication.accessToken,
    );
    User firebaseUser =
        (await firebaseAuth.signInWithCredential(credential)).user;

    if (firebaseUser != null) {
      final QuerySnapshot result = await FirebaseFirestore.instance
          .collection("users")
          .where("id", isEqualTo: firebaseUser.uid)
          .get();
      final List<DocumentSnapshot> documents = result.docs;
      if (documents.length == 0) {
        //New User, creates new data
        FirebaseFirestore.instance
            .collection("users")
            .doc(firebaseUser.uid)
            .set({
          "displayName": firebaseUser.displayName,
          "photoUrl": firebaseUser.photoURL,
          "id": firebaseUser.uid,
          "createdAt": Timestamp.now().toString(),
          "firstName": "",
          "lastName": "",
          "email": firebaseUser.email,
        });
        FirebaseFirestore.instance
            .collection("users")
            .doc(firebaseUser.uid)
            .collection("unassigned")
            .doc()
            .set({
          "glbRef": "PlaceHolder",
        });
        FirebaseFirestore.instance
            .collection("users")
            .doc(firebaseUser.uid)
            .collection("ListArmies")
            .doc()
            .set({
          "id": 1,
          "name": "unassigned",
        });
        //Writing data to local device
        currentUser = firebaseUser;

        await preferences.setString("id", currentUser.uid);
        await preferences.setString("displayName", currentUser.displayName);
        await preferences.setString("photoUrl", currentUser.photoURL);
        await preferences.setString("email", currentUser.email);
      } else {
        //Existing user, retrieves data
        await preferences.setString("id", documents[0]["id"]);
        await preferences.setString("displayName", documents[0]["displayName"]);
        await preferences.setString("photoUrl", documents[0]["photoUrl"]);
        await preferences.setString("email", documents[0]["email"]);
        await preferences.setString("firstName", documents[0]["firstName"]);
        await preferences.setString("lastName", documents[0]["lastName"]);
      }
      Fluttertoast.showToast(
        msg: "Sign in Successful",
        backgroundColor: Colors.grey,
        textColor: Colors.black,
      );
      this.setState(() {
        isLoading = false;
      });
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MainMenuScreen(),
        ),
      );
    }
    print("is this called?");
    setState(() {
      isLoading = false;
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
        title: Text(
          "Administratum AR",
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Center(
            child: TextButton(
              onPressed: _handleSignIn,
              child: Text(
                "SIGN IN WITH GOOGLE",
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
              style: TextButton.styleFrom(
                primary: Color(0xffdd4b39),
                shadowColor: Color(0xffff7f7f),
                backgroundColor: Color.fromRGBO(255, 81, 81, 1.0),
                padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
                textStyle: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Positioned(
            child: isLoading ? const Loading() : Container(),
          )
        ],
      ),
      backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
    );
  }
}

import 'package:administratum_ar/models/database_func_army.dart';
import 'package:administratum_ar/screens/main_menu_screen.dart';
import 'package:administratum_ar/screens/models_uploaded.dart';
import 'package:administratum_ar/screens/unit_details_selection.dart';
import 'package:administratum_ar/widgets/build_army_data.dart';
import 'package:administratum_ar/widgets/build_model_list.dart';
import 'package:administratum_ar/widgets/loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ArmyScreen extends StatefulWidget {
  final String armyName;

  ArmyScreen({Key key, @required this.armyName});

  @override
  _ArmyScreenState createState() => _ArmyScreenState(armyName: armyName);
}

class _ArmyScreenState extends State<ArmyScreen> {
  var fireInstance = FirebaseFirestore.instance;
  var fireAuth = FirebaseAuth.instance;
  bool isLoading = false;
  SharedPreferences sharedPreferences;
  String userId;
  String armyName;
  bool del;

  _ArmyScreenState({@required this.armyName});

  @override
  void initState() {
    super.initState();
    readLocal();
  }

  void readLocal() async {
    sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getString("id") ?? "";
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
          title: Text(
            "$armyName",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 20.0, top: 20.0),
              child: GestureDetector(
                onTap: () {
                  print("I was tapped");
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ModelsUploaded(armyName: armyName),
                    ),
                  );
                },
                child: Text("Add Custom"),
              ),
            )
          ],
          centerTitle: true,
        ),
        body: Stack(
          children: [new Container(child: _buildChild())],
        ),
        floatingActionButton: new FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => UnitDetailSelection(
                          userId: userId,
                          armyName: armyName,
                          modelName: "",
                          modelUrl: "",
                        )));
          },
          backgroundColor: Color.fromRGBO(255, 81, 81, 1.0),
          child: Icon(Icons.add),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
      ),
      onWillPop: onBackPress,
    );
  }

  Future<bool> onBackPress() {
    Navigator.of(context).pop();
    return Future.value(false);
  }

  Widget _buildChild() {
    if (armyName != "") {
      return new Container(
        child: StreamBuilder(
          stream: fireInstance
              .collection("users")
              .doc(userId)
              .collection(armyName)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                ),
              );
            } else if (snapshot.data.docs[0]["glbRef"] == "PlaceHolder") {
              return Center(child: Text("Your army is currently empty!"));
            } else {
              return snapshot.data.docs.length == 0
                  ? Center(
                      child: Text("Your army is currently empty!"),
                    )
                  : ListView.builder(
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (context, index) {
                        final unit = snapshot.data.docs[index].reference.id;
                        return Dismissible(
                          // Each Dismissible must contain a Key. Keys allow Flutter to
                          // uniquely identify widgets.
                          key: Key(unit),
                          // Provide a function that tells the app
                          // what to do after an item has been swiped away.
                          onDismissed: (direction) async {
                            if (snapshot.data.docs.length == 1) {
                              await fireInstance
                                  .collection("users")
                                  .doc(userId)
                                  .collection(armyName)
                                  .doc()
                                  .set({
                                "glbRef": "PlaceHolder",
                              });
                            }
                            await fireInstance.runTransaction(
                                (Transaction myTransaction) async {
                              await myTransaction
                                  .delete(snapshot.data.docs[index].reference);
                            });
                            // Remove the item from the data source.
                            setState(() {
                              unit.remove();
                            });
                          },
                          // Show a red background as the item is swiped away.
                          background: Container(
                            color: Colors.red,
                            alignment: Alignment.centerRight,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 0.0),
                              child: Icon(
                                Icons.delete_forever,
                                size: 40.0,
                              ),
                            ),
                          ),
                          child: buildArmyData(
                              context,
                              snapshot.data.docs[index],
                              userId,
                              armyName,
                              index),
                        );
                      },
                    );
            }
          },
        ),
      );
    }
    return Center(child: Text("No Army Selected!"));
  }
}

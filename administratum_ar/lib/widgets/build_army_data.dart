import 'dart:ffi';

import 'package:administratum_ar/screens/detail_screen.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'loading.dart';

Widget buildArmyData(
    BuildContext context, DocumentSnapshot documentSnapshot, String userId, String armyName, int index) {
  return Container(
    child: TextButton(
      child: Row(
        children: <Widget>[
          Material(
            child:CachedNetworkImage(
              imageUrl: documentSnapshot["unit"]["imageUrl"] == null
                  ? "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/PlaceholderImage%2Flogo.png?alt=media&token=a39bee97-d33f-496a-b749-88620d5bcf3a"
                  : documentSnapshot["unit"]["imageUrl"],
              placeholder: (context, url) => Loading(),
              width: 100,
              height: 100,
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(18.0),
            ),
            clipBehavior: Clip.hardEdge,
          ),
          Flexible(
            flex: 1,
            child: Container(
              child: documentSnapshot["modelName"] == "" ? Text(
                "${documentSnapshot["unit"]["name"]}",
                style: TextStyle(color: Colors.black),
              ) : Text(
                "${documentSnapshot["modelName"]}",
                style: TextStyle(color: Colors.black),
              ),
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(left: 10),
            ),
          ),
        ],
      ),
      onPressed: () {
        var wepIDs = [];
        for(int i = 0; i < documentSnapshot["weapons"].length; i++) {
          wepIDs.add(documentSnapshot["weapons"][i]["wID"]);
        }
        print(wepIDs);
        String docID = documentSnapshot.id;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailScreen(
                      unitId: documentSnapshot["unit"]["sheetId"], userId: userId, armyName: armyName, wepIDs: wepIDs, docID: docID,
                    )));
      },
    ),
  );
}

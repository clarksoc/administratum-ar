import 'package:cloud_firestore/cloud_firestore.dart';

var fireInstance = FirebaseFirestore.instance;

Future<List<String>> getArmyList(String userId) async {
  List<String> armies = [];
  print(userId);
  await fireInstance
      .collection('users')
      .doc(userId)
      .collection('ListArmies')
      .get()
      .then((QuerySnapshot querySnapshot) =>
  {
    querySnapshot.docs.forEach((doc) {
      armies.add(doc["name"]);
      //print(doc["name"]);
    })
  });

  return armies;
}
Future<List<Map<String, dynamic>>> getArmyListWithFaction(String userId) async {
  List<Map<String, dynamic>> armiesFaction = [];
  print(userId);
  await fireInstance
      .collection('users')
      .doc(userId)
      .collection('ListArmies')
      .get()
      .then((QuerySnapshot querySnapshot) =>
  {
    querySnapshot.docs.forEach((doc) {
      armiesFaction.add(doc["faction"]);
      //print(doc["name"]);
    })
  });

  return armiesFaction;
}

Future<List<Map<String, dynamic>>> getUnitDetailsList(String userId, String armyName) async {

  List<Map<String, dynamic>> unitDetails = [];

  await fireInstance.collection('users').doc(userId).collection(armyName)
      .get()
      .then((QuerySnapshot querySnapshot) {
    querySnapshot.docs.forEach((doc) {
      unitDetails.add(doc["unit"]);
    });
  });
  return unitDetails;
}
Future<List<List<dynamic>>> getUnitWeaponsList(String userId, String armyName) async {

  List<List<dynamic>> unitDetails = [];

  await fireInstance.collection('users').doc(userId).collection(armyName)
      .get()
      .then((QuerySnapshot querySnapshot) {
    querySnapshot.docs.forEach((doc) {
      unitDetails.add(doc["weapons"]);
    });
  });
  return unitDetails;
}


void addNewArmy(String userId, String armyName, String factionName) async {
  await FirebaseFirestore.instance
      .collection("users")
      .doc(userId)
      .collection(armyName)
      .doc()
      .set({
    "glbRef": "PlaceHolder",
  });

  //TODO Code Snip to implement an army cap
  //final QuerySnapshot qSnap = await FirebaseFirestore.instance.collection('users').doc(userId).collection("ListArmies").get();
  //int size = qSnap.size;

  await FirebaseFirestore.instance
      .collection("users")
      .doc(userId)
      .collection("ListArmies")
      .doc()
      .set({
    "name": armyName,
    "faction": factionName,
  });
}

import 'package:administratum_ar/models/database_func_army.dart';
import 'package:administratum_ar/models/database_func_models.dart';
import 'package:administratum_ar/models/unit_db_details.dart';
import 'package:administratum_ar/models/unit_model.dart';
import 'package:administratum_ar/widgets/loading.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';

class UnitDetailSelection extends StatefulWidget {
  final String userId;
  final String armyName;
  final String modelName;
  final String modelUrl;

  UnitDetailSelection(
      {Key key,
      @required this.userId,
      @required this.armyName,
      @required this.modelName,
      @required this.modelUrl});

  @override
  _UnitDetailSelectionState createState() => _UnitDetailSelectionState(
      userId: userId,
      armyName: armyName,
      modelName: modelName,
      modelUrl: modelUrl);
}

class _UnitDetailSelectionState extends State<UnitDetailSelection> {
  String userId;
  String armyName;
  String modelUrl;
  String modelName;
  String dropUnit = 'Select Unit';
  String dropWepA = 'Select Weapon A';
  String dropWepB = 'Select Weapon B';
  FactionClass armyFaction;
  UnitClass placeHolderUnit = UnitClass(
      no: '-',
      m: '-',
      ws: '-',
      bs: '-',
      s: '-',
      t: '-',
      w: '-',
      a: '-',
      ld: '-',
      sv: '-');
  UnitClass unit = UnitClass(
      no: '-',
      m: '-',
      ws: '-',
      bs: '-',
      s: '-',
      t: '-',
      w: '-',
      a: '-',
      ld: '-',
      sv: '-');
  WeaponClass weaponA = WeaponClass(
      name: "-",
      range: "-",
      type: "-",
      filterType: "-",
      str: "-",
      ap: "-",
      d: "-");
  WeaponClass weaponB = WeaponClass(
      name: "-",
      range: "-",
      type: "-",
      filterType: "-",
      str: "-",
      ap: "-",
      d: "-");
  WeaponClass weaponHolder = WeaponClass(
      name: "-",
      range: "-",
      type: "-",
      filterType: "-",
      str: "-",
      ap: "-",
      d: "-");

  _UnitDetailSelectionState({
    @required this.userId,
    @required this.armyName,
    @required this.modelName,
    @required this.modelUrl,
  });

  List<String> unitList = ['Select Unit'];
  List<String> weaponListA = ['Select Weapon A'];
  List<String> weaponListB = ['Select Weapon B'];
  List<UnitClass> unitClassList;
  List<WeaponClass> wepClassList;

  Future<String> getData() async {
    armyFaction = await getFaction(userId, armyName);
    unitClassList = await getUnitClassListByFaction(armyFaction.idName);
    List<String> tempUnit = makeUnitNameList(unitClassList);

    setState(() {
      unitList += tempUnit;
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
          title: Text(
            "Unit Details",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            Container(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
              color: Colors.black,
              width: double.infinity,
              child: Text('$dropUnit',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 18,
                  )),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
              child: Material(
                child: CachedNetworkImage(
                  imageUrl: unit.imageUrl == null
                      ? "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/PlaceholderImage%2Flogo.png?alt=media&token=a39bee97-d33f-496a-b749-88620d5bcf3a"
                      : unit.imageUrl,
                  placeholder: (context, url) => Loading(),
                  width: 150,
                  height: 150,
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(18.0),
                ),
                clipBehavior: Clip.hardEdge,
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 2),
              color: Colors.black45,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('No',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('M',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('WS',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('BS',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('S',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('T',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('W',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('A',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('Ld',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('Sv',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('${unit.no}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.m}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.ws}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.bs}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.s}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.t}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.w}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.a}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.ld}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${unit.sv}', textAlign: TextAlign.center),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
              margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
              color: Colors.black45,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('WEAPON',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('RANGE',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('TYPE',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('S',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('AP',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('D',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('${weaponA.name}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponA.range}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponA.type}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponA.str}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponA.ap}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponA.d}', textAlign: TextAlign.center),
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
              margin: EdgeInsets.fromLTRB(0, 2, 0, 2),
              color: Colors.black45,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('WEAPON',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('RANGE',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('TYPE',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('S',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('AP',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: Text('D',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('${weaponB.name}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponB.range}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponB.type}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponB.str}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponB.ap}', textAlign: TextAlign.center),
                ),
                Expanded(
                  child: Text('${weaponB.d}', textAlign: TextAlign.center),
                ),
              ],
            ),
            Padding(padding: EdgeInsets.fromLTRB(0, 15, 0, 0)),
            new DropdownButton(
              items: unitList.map<DropdownMenuItem<String>>((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList() ??
                  [],
              onChanged: (newVal) async {
                dropUnit = newVal;
                if (dropUnit != 'Select Unit') {
                  dropWepA = 'Select Weapon A';
                  dropWepB = 'Select Weapon B';

                  weaponListA = ['Select Weapon A'];
                  weaponListB = ['Select Weapon B'];

                  unit = unitClassList
                      .where((element) => element.name == dropUnit)
                      .first;
                  wepClassList = await getWeaponListByUnit(unitClassList
                      .where((element) => element.name == dropUnit)
                      .first
                      .sheetId);
                  weaponListA += makeWepNameList(wepClassList);
                  weaponListB += makeWepNameList(wepClassList);
                }
                setState(() {
                  weaponA = weaponHolder;
                  weaponB = weaponHolder;
                  if (dropUnit == 'Select Unit') {
                    unit = placeHolderUnit;
                    weaponA = weaponHolder;
                    weaponB = weaponHolder;

                    dropWepA = 'Select Weapon A';
                    dropWepB = 'Select Weapon B';

                    weaponListA = ['Select Weapon A'];
                    weaponListB = ['Select Weapon B'];
                  }
                });
              },
              value: dropUnit,
            ),
            new DropdownButton(
              items: weaponListA.map<DropdownMenuItem<String>>((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList() ??
                  [],
              onChanged: (newVal) {
                dropWepA = newVal;
                if (dropWepA != 'Select Weapon A') {
                  weaponA = wepClassList
                      .where((element) => element.name == dropWepA)
                      .first;
                }
                setState(() {
                  if (dropWepA == 'Select Weapon A') {
                    weaponA = weaponHolder;
                  }
                });
              },
              value: dropWepA,
            ),
            new DropdownButton(
              items: weaponListB.map<DropdownMenuItem<String>>((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList() ??
                  [],
              onChanged: (newVal) {
                dropWepB = newVal;
                if (dropWepB != 'Select Weapon B') {
                  weaponB = wepClassList
                      .where((element) => element.name == dropWepB)
                      .first;
                }
                setState(() {
                  if (dropWepB == 'Select Weapon B') {
                    weaponB = weaponHolder;
                  }
                });
              },
              value: dropWepB,
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
              child: ElevatedButton(
                child: Text("Add Unit to an Army"),
                onPressed: () {
                  if (dropUnit == 'Select Unit') {
                    Fluttertoast.showToast(
                      msg: "Must Select a Unit and at least one Weapon!",
                      backgroundColor: Colors.grey,
                      textColor: Colors.black,
                    );
                  } else if ((dropWepA == 'Select Weapon A') &&
                      (dropWepB == 'Select Weapon B')) {
                    Fluttertoast.showToast(
                      msg: "Must Select at least one Weapon!",
                      backgroundColor: Colors.grey,
                      textColor: Colors.black,
                    );
                  } else {
                    addToArmy();
                  }
                },
              ),
            ),
          ]),
        ),
        backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
      ),
      onWillPop: onBackPress,
    );
  }

  int count = 0;
  Future<bool> onBackPress() {
    modelUrl == "" ? Navigator.of(context).pop() : Navigator.of(context).popUntil((route) {
      return count++ == 2;
    });
    return Future.value(false);
  }

  List<String> makeUnitNameList(List<UnitClass> _unit) {
    List<String> temp = [];
    _unit.forEach((element) {
      temp.add(element.name);
    });
    return temp;
  }

  List<String> makeWepNameList(List<WeaponClass> _weapon) {
    List<String> temp = [];
    _weapon.forEach((element) {
      temp.add(element.name);
    });
    return temp;
  }

  void addToArmy() {
    if ((dropWepA == 'Select Weapon A') || (dropWepB == 'Select Weapon B')) {
      updateUnassignedUnit(
          userId,
          new UnitModel(
              bundleUrl: modelUrl,
              modelName: modelUrl == "" ? "" : "$modelName (Custom)",
              isCustom: modelUrl == "" ? false : true,
              unit: unitClassList
                  .where((element) => element.name == dropUnit)
                  .first,
              faction: armyFaction,
              weapons: [
                wepClassList.where((element) => element.name == dropWepA).first
              ]),
          armyName);

      Fluttertoast.showToast(
        msg: "Unit added to army",
        backgroundColor: Colors.grey,
        textColor: Colors.black,
      );
      setState(() {
        dropUnit = 'Select Unit';
        dropWepA = 'Select Weapon A';
        dropWepB = 'Select Weapon B';
        unit = placeHolderUnit;
        weaponA = weaponHolder;
        weaponB = weaponHolder;
      });
    } else {
      updateUnassignedUnit(
          userId,
          new UnitModel(
              bundleUrl: modelUrl,
              modelName: modelUrl == "" ? "" : "$modelName (Custom)",
              isCustom: modelUrl == "" ? false : true,
              unit: unitClassList
                  .where((element) => element.name == dropUnit)
                  .first,
              faction: armyFaction,
              weapons: [
                wepClassList.where((element) => element.name == dropWepA).first,
                wepClassList.where((element) => element.name == dropWepB).first
              ]),
          armyName);

      Fluttertoast.showToast(
        msg: "Unit added to army",
        backgroundColor: Colors.grey,
        textColor: Colors.black,
      );
      setState(() {
        dropUnit = 'Select Unit';
        dropWepA = 'Select Weapon A';
        dropWepB = 'Select Weapon B';
        unit = placeHolderUnit;
        weaponA = weaponHolder;
        weaponB = weaponHolder;
      });
    }
  }
}

import 'dart:io';

//import 'package:administratum_ar/screens/display_model_screen.dart.dart';
import 'package:administratum_ar/widgets/loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import 'display_model_screen.dart';

class UploadModelScreen extends StatefulWidget {
  @override
  _UploadModelScreenState createState() => _UploadModelScreenState();
}

class _UploadModelScreenState extends State<UploadModelScreen> {
  final _formKey = GlobalKey<FormState>();
  final fireStoreInstance = FirebaseFirestore.instance;

  SharedPreferences sharedPreferences;
  File objFile;
  bool isLoading = false;
  String userId;
  String assetBundleFileName;
  String binFileName;
  String assetBundleFileUrl;
  bool assetBundleUploaded = false;
  bool binUploaded = true;
  String assetFileName;
  String uploadResults;

  TextEditingController assetNameController;
  String assetName;

  @override
  void initState() {
    super.initState();
    readLocal();
  }

  readLocal() async {
    sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getString("id") ?? "";
    assetNameController = TextEditingController(text: assetName);
    setState(() {});
  }

  void _trySubmit() {
    if (assetName == null) {
      Fluttertoast.showToast(
          msg: "Please Enter the Asset Name",
          backgroundColor: Colors.grey,
          textColor: Colors.black);
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DisplayModelScreen(
                    assetBundleUrl: '$assetBundleFileUrl',
                    assetFileName: '$assetFileName',
                    assetName: '$assetName',
                  )));
    }
  }

  Future getAssetBundleFile() async {
    FilePickerResult result =
        await FilePicker.platform.pickFiles(type: FileType.any);

    uploadResults = result.names[0];

    print("What is the Result: ${uploadResults.substring(uploadResults.length - 12)}");
    if (result != null && uploadResults.substring(uploadResults.length - 12) == ".assetbundle") {
      PlatformFile fileName = result.files.single;
      File file = File(result.files.single.path);
      assetBundleFileName = fileName.name;
      assetBundleUploaded = true;
      uploadFile(fileName.name, file.path, file);
    }else{
      Fluttertoast.showToast(
          msg: "Please Upload a Proper .assetbundle File!",
          backgroundColor: Colors.grey,
          textColor: Colors.black);
      setState(() {
        assetBundleUploaded = false;
      });
    }
  }

  Future uploadFile(String fileName, String filePath, File file) async {
    setState(() {
      isLoading = true;
    });
    var uuid = Uuid();
    print("File Name: $fileName");
    String pathName = fileName.substring(0, fileName.indexOf("."));
    firebase_storage.Reference storageReference = firebase_storage
        .FirebaseStorage.instance
        .ref()
        .child("$userId/model/${DateTime.now()}/$pathName/$fileName");
    firebase_storage.UploadTask storageUploadTask =
        storageReference.putFile(file);

    firebase_storage.TaskSnapshot storageTaskSnapshot;

    storageUploadTask.whenComplete(() => null).then((value) {
      if (value != null) {
        storageTaskSnapshot = value;
        print("Storage Task: ${storageTaskSnapshot.ref.fullPath}");
        storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
          assetBundleFileUrl = downloadUrl;

          Fluttertoast.showToast(
              msg: "Upload Successful!",
              backgroundColor: Colors.grey,
              textColor: Colors.black);

          setState(() {
            isLoading = false;
            assetFileName = pathName;
            print(
                "Model Name: $assetFileName -----------------------------------------------------------");
            print(
                "Model URL: $assetBundleFileUrl -----------------------------------------------------------");
          });
        }).catchError((error) {
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(
            msg: error.toString(),
            backgroundColor: Colors.grey,
            textColor: Colors.black,
          );
          setState(() {
            isLoading = false;
          });
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Upload AssetBundle File"),
        backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
      ),
      body: Center(
        child: Column(
          children: [
            Padding(padding: EdgeInsets.fromLTRB(0, 100, 0, 10)),
            ElevatedButton(
              child: Text("Upload .assetbundle file"),
              onPressed: getAssetBundleFile,
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) {
                    if (states.contains(MaterialState.pressed))
                      return Color.fromRGBO(255, 81, 81, .75);
                    return Color.fromRGBO(
                        255, 81, 81, 1.0); // Use the component's default.
                  },
                ),
              ),
            ),
            Expanded(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    assetBundleFileName == null
                        ? Container(
                            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child: Text("Upload an Asset Bundle File"),
                          )
                        : Container(
                            padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                            child:
                                Text("Asset Bundle File: $assetBundleFileName"),
                          ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 25, 0, 25),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                      child: Text(
                          "Please enter the Asset name located inside the .assetBundle File \n(This is case sensitive)"),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 25, 0, 25),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: TextFormField(
                        key: ValueKey("assetName"),
                        validator: validateName,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          hintText: "Enter the asset name here",
                          contentPadding: EdgeInsets.all(5.0),
                          hintStyle: TextStyle(
                            color: Colors.grey[500],
                          ),
                        ),
                        controller: assetNameController,
                        onChanged: (value) {
                          assetName = value;
                        },
                        onSaved: (value) {
                          assetName = value;
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
            isLoading
                ? const Loading()
                : !assetBundleUploaded
                    ? Container()
                    : ElevatedButton(
                        onPressed: _trySubmit,
                        child: Text("Continue"),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                      (Set<MaterialState> states) {
                    if (states.contains(MaterialState.pressed))
                      return Color.fromRGBO(52, 99, 119, 1.0);
                    return Color.fromRGBO(52, 99, 119, 0.75); // Use the component's default.
                  },
                ),              ),
                      ),
            Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 10)),
          ],
        ),
      ),
      backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
    );
  }
}

String validateName(String value) {
  if (value.length <= 1)
    return 'Name must not be empty!';
  else
    return null;
}

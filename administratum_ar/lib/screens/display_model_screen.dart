import 'package:administratum_ar/screens/main_menu_screen.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';


class DisplayModelScreen extends StatefulWidget {
  final String assetBundleUrl;
  final String assetFileName;
  final String assetName;

  DisplayModelScreen(
      {Key key, @required this.assetBundleUrl, @required this.assetFileName, @required this.assetName});

  @override
  _DisplayModelScreenState createState() =>
      _DisplayModelScreenState(assetBundleUrl: assetBundleUrl, assetFileName: assetFileName, assetName: assetName);
}

class _DisplayModelScreenState extends State<DisplayModelScreen> with WidgetsBindingObserver {

  SharedPreferences sharedPreferences;
  String assetBundleUrl;
  String assetFileName;
  String assetName;
  String userId;
  List<String> assetArray;
  String assetCat;

  UnityWidgetController _unityWidgetController;
  static final GlobalKey<ScaffoldState> _scaffoldKey =
  GlobalKey<ScaffoldState>();

  _DisplayModelScreenState({@required this.assetBundleUrl, @required this.assetFileName, @required this.assetName});

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    readLocal();
  }

  @override
  void dispose() {
    _unityWidgetController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        onUnityCreated(_unityWidgetController);
        print("app resumed");
        break;
      case AppLifecycleState.inactive:
        print("app inactive");
        pauseUnity();
        break;
      case AppLifecycleState.paused:
        pauseUnity();
        print("app paused");
        break;
      case AppLifecycleState.detached:
        pauseUnity();
        print("app detached");
        break;
    }
    setState(() {});
  }

  pauseUnity() async{
    await _unityWidgetController.pause();
  }

  readLocal() async {
    sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getString("id") ?? "";
    print(
        "assetFileName: $assetFileName -------------------------------------------------------");
    print(
        "assetBundleUrl: $assetBundleUrl -------------------------------------------------------");
    print(
        "assetName: $assetName -------------------------------------------------------");
    assetArray = ['$assetBundleUrl', '$assetName'];
    assetCat = "$assetBundleUrl|$assetName|null|null";

    setState(() {});
  }

  void saveModel() async {
    FirebaseFirestore.instance
        .collection("users")
        .doc(userId)
        .collection("models")
        .doc("${DateTime.now()}$assetFileName")
        .set({
      "assetBundleUrl": assetBundleUrl,
      "assetFileName": assetFileName,
      "assetName": assetName,
    });
    Fluttertoast.showToast(
        msg: "Save Successful!",
        backgroundColor: Colors.grey,
        textColor: Colors.black);
    _unityWidgetController.postMessage("Scripts", 'ReloadScene', 'Scene Reloaded');
    quitUnity(_unityWidgetController);
    await _unityWidgetController.pause();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => MainMenuScreen()));
  }

  Future deleteModel() async {
/*    firebase_storage.Reference storageReference = firebase_storage
        .FirebaseStorage.instance
        .ref()
        .child("$userId/model/$assetFileName/$assetFileName.assetbundle");
    await storageReference.delete();
    Fluttertoast.showToast(
        msg: "Deletion Successful!",
        backgroundColor: Colors.grey,
        textColor: Colors.black);*/
    _unityWidgetController.postMessage("Scripts", 'ReloadScene', 'Scene Reloaded');
    quitUnity(_unityWidgetController);
    await _unityWidgetController.pause();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => MainMenuScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
/*        await _unityWidgetController.pause();
        deleteModel();
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MainScreen()));*/
        return false;
      },
      child: MaterialApp(
        home: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            centerTitle: true,
            title: const Text('Upload Model'),
            backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
          ),
          body: Card(
            margin: const EdgeInsets.all(8),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Stack(
              children: <Widget>[
                UnityWidget(
                  onUnityCreated: onUnityCreated,
                  onUnityMessage: onUnityMessage,
                  onUnitySceneLoaded: onUnitySceneLoaded,
                  fullscreen: false,
                  borderRadius: BorderRadius.circular(2.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FloatingActionButton.extended(
                      onPressed: () => saveModel(),
                      label: Text("SAVE"),
                      heroTag: "buttonSave",
                      backgroundColor: Color.fromRGBO(52, 99, 119, 1.0),

                    ),
                    FloatingActionButton.extended(
                      onPressed: () => deleteModel(),
                      label: Text("CANCEL"),
                      heroTag: "buttonDelete",
                      backgroundColor: Color.fromRGBO(255, 81, 81, 1.0),
                    ),
                  ],
                ),
              ],
            ),
          ),
          backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
        ),
      ),
    );
  }

  // Communication from Unity to Flutter
  void onUnityMessage(message) {
    print('Received message from unity: ${message.toString()}');
  }

  // Callback that connects the created controller to the unity controller
  void onUnityCreated(UnityWidgetController controller) async {
    this._unityWidgetController = controller;
    //this._unityWidgetController.postMessage("Scripts", 'LoadScene', 'ARTest');
    this._unityWidgetController.postMessage("Scripts", 'LoadModel', '$assetCat');
    //this._unityWidgetController.postMessage("Scripts", 'LoadModel', '$modelUrl');
    //this._unityWidgetController.postMessage("LoadModelScript", 'SelectScene', 'ARTest');
    final _isPaused = await this._unityWidgetController.isPaused();
    if(_isPaused){
      Future.delayed(
        Duration(milliseconds: 50),
            () async {
              await this._unityWidgetController.resume();
            },
      );
    }

  }

  // Communication from Unity when new scene is loaded to Flutter
  void onUnitySceneLoaded(SceneLoaded sceneInfo) {
    print('Received scene loaded from unity: ${sceneInfo.name}');
    print('Received scene loaded from unity buildIndex: ${sceneInfo.buildIndex}');
    //this._unityWidgetController.postMessage("Scripts", 'LoadModel', '$modelUrl');
  }

  void quitUnity(UnityWidgetController controller) async{
    this._unityWidgetController = controller;
    this._unityWidgetController.postMessage("Scripts", "DeleteAllUnits", "");
  }

}
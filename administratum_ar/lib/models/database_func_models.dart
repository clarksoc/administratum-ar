import 'dart:ffi';

import 'package:administratum_ar/models/unit_db_details.dart';
import 'package:administratum_ar/models/unit_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

var fireInstance = FirebaseFirestore.instance;

Future<FactionClass> getFaction(String userId, String armyName) async {
  FactionClass faction;
  var armySnap;
  await fireInstance
      .collection("users")
      .doc(userId)
      .collection("ListArmies")
      .where("name", isEqualTo: armyName)
      .get()
      .then((snapshot) {
    armySnap = snapshot.docs.first;
  });

  await fireInstance
      .collection('Factions')
      .where('name', isEqualTo: armySnap["faction"])
      .get()
      .then((snapshot) {
    faction = new FactionClass(
        id: snapshot.docs.first["id"], name: snapshot.docs.first["name"], idName: snapshot.docs.first["idName"]);
  });

  return faction;
}

Future<String> getFactionByUnitID(int sheetId) async {
  String fID;
  String faction;
  await fireInstance
      .collection('Unit-Faction')
      .where('sheetId', isEqualTo: sheetId)
      .get()
      .then((QuerySnapshot querySnapshot) => {
            querySnapshot.docs.forEach((doc) {
              fID = doc["fID"];
            })
          });

  if (fID != null) {
    await fireInstance
        .collection('Factions')
        .where('idName', isEqualTo: fID)
        .get()
        .then((QuerySnapshot querySnapshot) => {
              querySnapshot.docs.forEach((doc) {
                faction = doc["name"];
              })
            });
  }
  return faction;
}

Future<List<String>> makeFactionNameList() async {
  List<FactionClass> faction = await getFactionList();
  List<String> temp = [];
  faction.forEach((element) {
    temp.add(element.name);
  });
  return temp;
}

Future<List<FactionClass>> getFactionList() async {
  List<FactionClass> factions = [];
  await fireInstance
      .collection('Factions')
      .orderBy('id')
      .get()
      .then((QuerySnapshot querySnapshot) => {
            querySnapshot.docs.forEach((doc) {
              factions.add(new FactionClass(
                  id: doc["id"], name: doc["name"], idName: doc["idName"]));
            })
          });
  return factions;
}

Future<List<UnitClass>> getUnitClassListByFaction(String fID) async {
  List<UnitClass> units = [];
  List<int> sheetId = [];

  if (fID != null) {
    await fireInstance
        .collection('Unit-Faction')
        .where('fID', isEqualTo: fID)
        .get()
        .then((QuerySnapshot querySnapshot) => {
              querySnapshot.docs.forEach((doc) {
                sheetId.add(doc["sheetId"]);
              })
            });
  }

  if (sheetId != null) {
    await fireInstance
        .collection('Units')
        .get()
        .then((QuerySnapshot querySnapshot) => {
              querySnapshot.docs.forEach((doc) {
                if (sheetId.contains(doc["sheetId"])) {
                  units.add(new UnitClass(
                      id: doc["id"],
                      sheetId: doc["sheetId"],
                      imageUrl: doc["imageUrl"],
                      name: doc["name"],
                      no: doc["no"],
                      m: doc["m"],
                      ws: doc["ws"],
                      bs: doc["bs"],
                      s: doc["s"],
                      t: doc["t"],
                      w: doc["w"],
                      a: doc["a"],
                      ld: doc["ld"],
                      sv: doc["sv"]));
                }
              })
            });
  }
  return units;
}

Future<UnitClass> getUnitClassById(int sheetId) async {
  UnitClass unit;

  if (sheetId != null) {
    await fireInstance
        .collection('Units')
        .where('sheetId', isEqualTo: sheetId)
        .get()
        .then((snapshot) {
      var doc = snapshot.docs.first;
      unit = new UnitClass(
          id: doc["id"],
          sheetId: doc["sheetId"],
          imageUrl: doc["imageUrl"],
          name: doc["name"],
          no: doc["no"],
          m: doc["m"],
          ws: doc["ws"],
          bs: doc["bs"],
          s: doc["s"],
          t: doc["t"],
          w: doc["w"],
          a: doc["a"],
          ld: doc["ld"],
          sv: doc["sv"]);
    });
  }
  return unit;
}

Future<List<WeaponClass>> getWeaponListByUnit(int sheetId) async {
  List<WeaponClass> weapon = [];
  List<int> wID = [];

  if (sheetId != null) {
    await fireInstance
        .collection('Unit-Weapon')
        .where('sheetId', isEqualTo: sheetId)
        .get()
        .then((QuerySnapshot querySnapshot) => {
              querySnapshot.docs.forEach((doc) {
                wID.add(doc["wID"]);
              })
            });
  }

  if (wID != null) {
    await fireInstance
        .collection('Weapons')
        .get()
        .then((QuerySnapshot querySnapshot) => {
              querySnapshot.docs.forEach((doc) {
                if (wID.contains(doc["wID"])) {
                  weapon.add(new WeaponClass(
                      id: doc["id"],
                      wID: doc["wID"],
                      name: doc["name"],
                      range: doc["range"],
                      type: doc["type"],
                      str: doc["str"],
                      ap: doc["ap"],
                      d: doc["d"],
                      filterType: doc["filterType"]));
                }
              })
            });
  }
  return weapon;
}

void updateUnassignedUnit(
    String userId, UnitModel unitModel, String armyName) async {
  QueryDocumentSnapshot QDS;
  String newRef;
  var temp;

  await fireInstance
      .collection("users")
      .doc(userId)
      .collection(armyName)
      .get()
      .then((value) async => {
            if (value.docs.first["glbRef"] == "PlaceHolder")
              {
                QDS = value.docs.first,
                newRef = QDS.id,
                fireInstance
                    .collection("users")
                    .doc(userId)
                    .collection(armyName)
                    .doc(newRef)
                    .delete(),
                if (armyName != "unassigned")
                  {
                    await fireInstance
                        .collection('users')
                        .doc(userId)
                        .collection('ListArmies')
                        .where('name', isEqualTo: armyName)
                        .get()
                        .then((QuerySnapshot querySnapshot) =>
                            {temp = querySnapshot.docs.first.reference.id}),
                    await fireInstance
                        .collection('users')
                        .doc(userId)
                        .collection('ListArmies')
                        .doc(temp)
                        .update({'faction': unitModel.faction.name}),
                  }
              }
            else
              {print("no placeholder")}
          });

  if (unitModel.weapons.length > 1) {
    await fireInstance
        .collection("users")
        .doc(userId)
        .collection(armyName)
        .doc()
        .set({
      "faction": {
        "id": unitModel.faction.id,
        "idName": unitModel.faction.idName,
        "name": unitModel.faction.name
      },
      "unit": {
        "id": unitModel.unit.id,
        "sheetId": unitModel.unit.sheetId,
        "imageUrl": unitModel.unit.imageUrl,
        "name": unitModel.unit.name,
        "no": unitModel.unit.no,
        "m": unitModel.unit.m,
        "ws": unitModel.unit.ws,
        "bs": unitModel.unit.bs,
        "s": unitModel.unit.s,
        "t": unitModel.unit.t,
        "w": unitModel.unit.w,
        "a": unitModel.unit.a,
        "ld": unitModel.unit.ld,
        "sv": unitModel.unit.sv,
      },
      "weapons": [
        {
          "id": unitModel.weapons[0].id,
          "wID": unitModel.weapons[0].wID,
          "name": unitModel.weapons[0].name,
          "range": unitModel.weapons[0].range,
          "type": unitModel.weapons[0].type,
          "filterType": unitModel.weapons[0].filterType,
          "str": unitModel.weapons[0].str,
          "ap": unitModel.weapons[0].ap,
          "d": unitModel.weapons[0].d,
        },
        {
          "id": unitModel.weapons[1].id,
          "wID": unitModel.weapons[1].wID,
          "name": unitModel.weapons[1].name,
          "range": unitModel.weapons[1].range,
          "type": unitModel.weapons[1].type,
          "filterType": unitModel.weapons[1].filterType,
          "str": unitModel.weapons[1].str,
          "ap": unitModel.weapons[1].ap,
          "d": unitModel.weapons[1].d,
        }
      ],
      //"glbRef": unitModel.glbRef
      "glbRef":
          "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/Basalisk%2Fscene.glb?alt=media&token=21f66882-d788-4023-8213-db2094b151f4",
      "isCustom": unitModel.isCustom,
      "bundleUrl": unitModel.bundleUrl,
      "modelName": unitModel.modelName,

    });
  } else {
    await fireInstance
        .collection("users")
        .doc(userId)
        .collection(armyName)
        .doc()
        .set({
      "faction": {
        "id": unitModel.faction.id,
        "idName": unitModel.faction.idName,
        "name": unitModel.faction.name
      },
      "unit": {
        "id": unitModel.unit.id,
        "sheetId": unitModel.unit.sheetId,
        "imageUrl": unitModel.unit.imageUrl,
        "name": unitModel.unit.name,
        "no": unitModel.unit.no,
        "m": unitModel.unit.m,
        "ws": unitModel.unit.ws,
        "bs": unitModel.unit.bs,
        "s": unitModel.unit.s,
        "t": unitModel.unit.t,
        "w": unitModel.unit.w,
        "a": unitModel.unit.a,
        "ld": unitModel.unit.ld,
        "sv": unitModel.unit.sv,
      },
      "weapons": [
        {
          "id": unitModel.weapons[0].id,
          "wID": unitModel.weapons[0].wID,
          "name": unitModel.weapons[0].name,
          "range": unitModel.weapons[0].range,
          "type": unitModel.weapons[0].type,
          "filterType": unitModel.weapons[0].filterType,
          "str": unitModel.weapons[0].str,
          "ap": unitModel.weapons[0].ap,
          "d": unitModel.weapons[0].d,
        }
      ],
      //"glbRef": unitModel.glbRef
      "glbRef":
          "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/Basalisk%2Fscene.glb?alt=media&token=21f66882-d788-4023-8213-db2094b151f4",
      "isCustom": unitModel.isCustom,
      "bundleUrl": unitModel.bundleUrl,
      "modelName": unitModel.modelName,
    });
  }
}

void updateUnitWeapons(String userId, String armyName, String docID,
    List<WeaponClass> weapons) async {
  DocumentSnapshot<Map<String, dynamic>> updateUnit = await fireInstance
      .collection("users")
      .doc(userId)
      .collection(armyName)
      .doc(docID)
      .snapshots()
      .first;

  updateUnit.reference.update({
    'weapons': FieldValue.arrayRemove(updateUnit.get("weapons")),
  });

  if (weapons.length == 0) {
    updateUnit.reference.update({
      'weapons': FieldValue.arrayUnion([
        {
          "id": weapons[0].id,
          "wID": weapons[0].wID,
          "name": weapons[0].name,
          "range": weapons[0].range,
          "type": weapons[0].type,
          "filterType": weapons[0].filterType,
          "str": weapons[0].str,
          "ap": weapons[0].ap,
          "d": weapons[0].d,
        }
      ]),
    });
  } else {
    updateUnit.reference.update({
      'weapons': FieldValue.arrayUnion([
        {
          "id": weapons[0].id,
          "wID": weapons[0].wID,
          "name": weapons[0].name,
          "range": weapons[0].range,
          "type": weapons[0].type,
          "filterType": weapons[0].filterType,
          "str": weapons[0].str,
          "ap": weapons[0].ap,
          "d": weapons[0].d,
        },
        {
          "id": weapons[1].id,
          "wID": weapons[1].wID,
          "name": weapons[1].name,
          "range": weapons[1].range,
          "type": weapons[1].type,
          "filterType": weapons[1].filterType,
          "str": weapons[1].str,
          "ap": weapons[1].ap,
          "d": weapons[1].d,
        }
      ])
    });
  }
}

import 'package:administratum_ar/screens/unit_details_selection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

Widget buildModelList(BuildContext context, DocumentSnapshot documentSnapshot,
    int index, String armyName, String userId) {
  print("Army Name: " + armyName);
  return Container(
    child: TextButton(
      child: Row(
        children: [
/*          Material(
            child: Icon(
              Icons.android_sharp,
              size: 50,
            ),
          ),*/
          Expanded(
            child: Center(
                child: Text(
              "${documentSnapshot["assetName"]}",
              style:
                  TextStyle(color: Color.fromRGBO(4, 9, 43, 1.0), fontSize: 20),
            )),
          ),
        ],
      ),
      onPressed: armyName == ""
          ? () {}
          : () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UnitDetailSelection(
                            userId: userId,
                            armyName: armyName,
                            modelUrl: documentSnapshot["assetBundleUrl"],
                            modelName: documentSnapshot["assetName"],
                          )));
            },
    ),
    padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
    decoration: BoxDecoration(
      border: Border.all(color: Colors.black38),
    ),
  );
}

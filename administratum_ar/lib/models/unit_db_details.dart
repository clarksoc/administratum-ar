import 'dart:core';
import 'package:flutter/material.dart';

class FactionClass {
  final int id;
  final String idName;
  final String name;

  FactionClass({@required this.id, @required this.idName, @required this.name});
}

class UnitClass {
  final int id;
  final int sheetId;
  final String imageUrl;
  final String name;
  final String no;
  final String m;
  final String ws;
  final String bs;
  final String s;
  final String t;
  final String w;
  final String a;
  final String ld;
  final String sv;

  UnitClass({
    @required this.id,
    @required this.sheetId,
    @required this.imageUrl,
    @required this.name,
    @required this.no,
    @required this.m,
    @required this.ws,
    @required this.bs,
    @required this.s,
    @required this.t,
    @required this.w,
    @required this.a,
    @required this.ld,
    @required this.sv,
  });
}

class WeaponClass {
  final int id;
  final int wID;
  final String name;
  final String range;
  final String type;
  final String filterType;
  final String str;
  final String ap;
  final String d;

  WeaponClass({
    @required this.id,
    @required this.wID,
    @required this.name,
    @required this.range,
    @required this.type,
    @required this.filterType,
    @required this.str,
    @required this.ap,
    @required this.d,
  });
}

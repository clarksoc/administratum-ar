import 'package:administratum_ar/widgets/build_model_list.dart';
import 'package:administratum_ar/widgets/loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ModelsUploaded extends StatefulWidget {
  String armyName;
  ModelsUploaded({Key key, @required this.armyName});

  @override
  _ModelsUploadedState createState() => _ModelsUploadedState(armyName: armyName);
}

class _ModelsUploadedState extends State<ModelsUploaded> {
  var fireInstance = FirebaseFirestore.instance;
  String userId;
  SharedPreferences sharedPreferences;
  String armyName;

  _ModelsUploadedState({@required this.armyName});

  @override
  void initState() {
    super.initState();
    readLocal();
  }

  readLocal() async {
    sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getString("id") ?? "";
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Uploaded Models List"),
        backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
      ),
      body: Stack(
        children: [
          Container(
            child: StreamBuilder(
              stream: fireInstance
                  .collection("users")
                  .doc(userId)
                  .collection("models")
                  .snapshots(),
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return Loading();
                } else {
                  return snapshot.data.docs.length == 0
                      ? Center(
                          child: Text("No Uploaded Models Yet!"),
                        )
                      : ListView.builder(
                          itemBuilder: (context, index) => buildModelList(
                              context, snapshot.data.docs[index], index, armyName, userId),
                          itemCount: snapshot.data.docs.length,
                        );
                }
              },
            ),
          )
        ],
      ),
      backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
    );
  }
}

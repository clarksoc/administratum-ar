import 'package:administratum_ar/screens/login_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

import 'main_menu_screen.dart';

class LaunchScreen extends StatefulWidget {
  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences sharedPreferences;

  bool isLoading = false;
  bool isLogIn = false;

  User currentUser;

  @override
  void initState() {
    super.initState();
    isSignedIn();
  }

  void isSignedIn() async {
    setState(() {
      isLoading = true;
    });

    sharedPreferences = await SharedPreferences.getInstance();
    isLogIn = await googleSignIn.isSignedIn();

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: isLogIn == true ? MainMenuScreen() : LoginScreen(),
/*      title: Text(
        "Administratum AR",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
        ),
      ),*/
      backgroundColor: Colors.white,
      //image: Image.network("https://i.ibb.co/DQKvvbC/logo.png"),
      imageBackground: Image.network("https://i.ibb.co/DQKvvbC/logo.png").image,
      onClick: () => print("I'm loading here!"),
      loaderColor: Colors.blue,
    );
  }
}

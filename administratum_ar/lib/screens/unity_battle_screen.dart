import 'package:administratum_ar/models/database_func_army.dart';
import 'package:administratum_ar/screens/main_menu_screen.dart';
import 'package:administratum_ar/widgets/open_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'dart:convert';

class UnityBattleScreen extends StatefulWidget {
  final String armyOne;
  final String armyTwo;

  UnityBattleScreen({
    Key key,
    @required this.armyOne,
    @required this.armyTwo,
  });

  @override
  _UnityBattleScreenState createState() =>
      _UnityBattleScreenState(armyOne: armyOne, armyTwo: armyTwo);
}

class _UnityBattleScreenState extends State<UnityBattleScreen>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  AnimationController animationController;
  Animation degOneTranslationAnimation,
      degTwoTranslationAnimation,
      degThreeTranslationAnimation;
  Animation rotationAnimation;

  String armyOne;
  String armyTwo;

  _UnityBattleScreenState({@required this.armyOne, @required this.armyTwo});

  String armyOneFaction;
  String armyTwoFaction;
  List<Map<String, dynamic>> armyOneUnits;
  List<Map<String, dynamic>> armyTwoUnits;

  List<String> armyOneCustomModelNames = [];
  List<String> armyTwoCustomModelNames = [];
  List<String> armyOneCustomModelUrls = [];
  List<String> armyTwoCustomModelUrls = [];

  List<List<dynamic>> armyOneWeapons;
  List<List<dynamic>> armyTwoWeapons;

  SharedPreferences sharedPreferences;
  String assetBundleUrl;
  String assetFileName;
  String assetName;
  String userId;
  List<String> assetArray;
  String assetCat;
  String guardArmy, marineArmy, necronArmy, tauArmy;
  firebase_storage.FirebaseStorage storage =
      firebase_storage.FirebaseStorage.instance;
  Map<String, String> bundleUrls = {};

  String screenTitle = "Battle!";

  String weapon = "";
  String unit = "";

  UnityWidgetController _unityWidgetController;
  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();

  @override
  void initState() {
    animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 250));
    degOneTranslationAnimation = TweenSequence(<TweenSequenceItem>[
      TweenSequenceItem<double>(
          tween: Tween<double>(begin: 0.0, end: 1.2), weight: 75.0),
      TweenSequenceItem<double>(
          tween: Tween<double>(begin: 1.2, end: 1.0), weight: 25.0),
    ]).animate(animationController);
    degTwoTranslationAnimation = TweenSequence(<TweenSequenceItem>[
      TweenSequenceItem<double>(
          tween: Tween<double>(begin: 0.0, end: 1.4), weight: 55.0),
      TweenSequenceItem<double>(
          tween: Tween<double>(begin: 1.4, end: 1.0), weight: 45.0)
    ]).animate(animationController);
    degThreeTranslationAnimation = TweenSequence(<TweenSequenceItem>[
      TweenSequenceItem<double>(
          tween: Tween<double>(begin: 0.0, end: 1.75), weight: 35.0),
      TweenSequenceItem<double>(
          tween: Tween<double>(begin: 1.75, end: 1.0), weight: 65.0)
    ]).animate(animationController);
    rotationAnimation = Tween<double>(begin: 180.0, end: 0.0).animate(
        CurvedAnimation(parent: animationController, curve: Curves.easeOut));
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    animationController.addListener(() {
      setState(() {});
    });
    readLocal();
  }

  @override
  void dispose() {
    animationController.dispose();
    _unityWidgetController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  AppLifecycleState _notification;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        onUnityCreated(_unityWidgetController);
        print("app resumed");
        break;
      case AppLifecycleState.inactive:
        print("app inactive");
        pauseUnity();
        break;
      case AppLifecycleState.paused:
        pauseUnity();
        print("app paused");
        break;
      case AppLifecycleState.detached:
        pauseUnity();
        print("app detached");
        break;
    }
    setState(() {
      _notification = state;
    });
  }

  readLocal() async {
    sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getString("id") ?? "";
    assetCat = "$assetBundleUrl|$assetName";
    await fireInstance
        .collection('users')
        .doc(userId)
        .collection(armyOne)
        .get()
        .then((QuerySnapshot querySnapshot) => {
              armyOneFaction = querySnapshot.docs.first['faction']['name'],
              print(
                  "$armyOneFaction ARMY ONE FACTION~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            });
    await fireInstance
        .collection('users')
        .doc(userId)
        .collection(armyTwo)
        .get()
        .then((QuerySnapshot querySnapshot) => {
              armyTwoFaction = querySnapshot.docs.first['faction']['name'],
              print(
                  "$armyTwoFaction ARMY TWO FACTION~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
            });
    await fireInstance
        .collection('users')
        .doc(userId)
        .collection(armyOne)
        .get()
        .then((QuerySnapshot querySnapshot) => {
              querySnapshot.docs.forEach((doc) {
                armyOneCustomModelNames.add(doc["modelName"]);
              })
            });
    await fireInstance
        .collection('users')
        .doc(userId)
        .collection(armyTwo)
        .get()
        .then((QuerySnapshot querySnapshot) => {
      querySnapshot.docs.forEach((doc) {
        armyTwoCustomModelNames.add(doc["modelName"]);
      })
    });
    await fireInstance
        .collection('users')
        .doc(userId)
        .collection(armyOne)
        .get()
        .then((QuerySnapshot querySnapshot) => {
      querySnapshot.docs.forEach((doc) {
        armyOneCustomModelUrls.add(doc["bundleUrl"]);
      })
    });
    await fireInstance
        .collection('users')
        .doc(userId)
        .collection(armyTwo)
        .get()
        .then((QuerySnapshot querySnapshot) => {
      querySnapshot.docs.forEach((doc) {
        armyTwoCustomModelUrls.add(doc["bundleUrl"]);
      })
    });

    print("$armyOneCustomModelNames Army One Custom Names~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    print("$armyTwoCustomModelNames Army Two Custom Names~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    print("$armyOneCustomModelUrls Army One Custom Urls~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    print("$armyTwoCustomModelUrls Army Two Custom Urls~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    armyOneUnits = await getUnitDetailsList(userId, armyOne);
    armyTwoUnits = await getUnitDetailsList(userId, armyTwo);
    print(
        "$armyOneUnits Army One Units~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    print(
        "$armyTwoUnits Army Two Units~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    armyOneWeapons = await getUnitWeaponsList(userId, armyOne);
    armyTwoWeapons = await getUnitWeaponsList(userId, armyTwo);
    print(
        "$armyOneWeapons Army One Weapons~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    print(
        "$armyTwoWeapons Army Two Weapons~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

    setState(() {
      bundleUrls = {
        "Astra Militarum":
            "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/AssetBundle%2Fimperialguard.assetbundle?alt=media&token=7ea8063b-aed7-4203-8fa6-4fe3ca69cb72",
        "Space Marines":
            "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/AssetBundle%2Fspacemarine.assetbundle?alt=media&token=b84b4289-f434-4378-aaaa-9f543c8cec90",
        "Necrons":
            "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/AssetBundle%2Fnecron.assetbundle?alt=media&token=472d1fd5-02c3-46e1-9ad5-c405af53c331",
        "T’au Empire":
            "https://firebasestorage.googleapis.com/v0/b/administratum-testdb.appspot.com/o/AssetBundle%2Ftau.assetbundle?alt=media&token=8bf0c759-4a5c-42cd-bd1c-04b14a47d47f"
      };
    });
  }

  pauseUnity() async {
    await _unityWidgetController.pause();
  }

  double getRadiansFromDegree(double degree) {
    double unitRadian = 57.295779513;
    return degree / unitRadian;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        quitUnity(_unityWidgetController);
        pauseUnity();
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MainMenuScreen()));
        return true;
      },
      child: MaterialApp(
        home: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
            title: new Text(screenTitle),
            centerTitle: true,
          ),
          body: Card(
            margin: const EdgeInsets.all(8),
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Stack(
              children: <Widget>[
                UnityWidget(
                  onUnityCreated: onUnityCreated,
                  onUnityMessage: onUnityMessage,
                  onUnitySceneLoaded: onUnitySceneLoaded,
                  fullscreen: false,
                  borderRadius: BorderRadius.circular(2.0),
                ),
                Positioned(
                    right: 30,
                    bottom: 30,
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: <Widget>[
                        IgnorePointer(
                          child: Container(
                            color: Colors.transparent,
                            height: 150.0,
                            width: 150.0,
                          ),
                        ),
                        Transform.translate(
                          offset: Offset.fromDirection(
                              getRadiansFromDegree(270),
                              degOneTranslationAnimation.value * 100),
                          child: Transform(
                            transform: Matrix4.rotationZ(
                                getRadiansFromDegree(rotationAnimation.value))
                              ..scale(degOneTranslationAnimation.value),
                            alignment: Alignment.center,
                            child: CircularButton(
                              color: Colors.blue,
                              width: 50,
                              height: 50,
                              icon: Icon(
                                Icons.filter_center_focus,
                                color: Colors.white,
                              ),
                              onClick: () {
                                print('First Button');
                                _setPhase("2");
                                animationController.reverse();
                                setState(() {
                                  screenTitle = "Attack Phase";
                                });
                              },
                            ),
                          ),
                        ),
                        Transform.translate(
                          offset: Offset.fromDirection(
                              getRadiansFromDegree(225),
                              degTwoTranslationAnimation.value * 100),
                          child: Transform(
                            transform: Matrix4.rotationZ(
                                getRadiansFromDegree(rotationAnimation.value))
                              ..scale(degTwoTranslationAnimation.value),
                            alignment: Alignment.center,
                            child: CircularButton(
                              color: Colors.black,
                              width: 50,
                              height: 50,
                              icon: Icon(
                                Icons.directions_run,
                                color: Colors.white,
                              ),
                              onClick: () {
                                print('Second button');
                                _setPhase("1");
                                animationController.reverse();
                                setState(() {
                                  screenTitle = "Movement Phase";
                                });
                              },
                            ),
                          ),
                        ),
                        Transform.translate(
                          offset: Offset.fromDirection(
                              getRadiansFromDegree(180),
                              degThreeTranslationAnimation.value * 100),
                          child: Transform(
                            transform: Matrix4.rotationZ(
                                getRadiansFromDegree(rotationAnimation.value))
                              ..scale(degThreeTranslationAnimation.value),
                            alignment: Alignment.center,
                            child: CircularButton(
                              color: Colors.orangeAccent,
                              width: 50,
                              height: 50,
                              icon: Icon(
                                Icons.pin_drop,
                                color: Colors.white,
                              ),
                              onClick: () {
                                print('Third Button');
                                _setPhase("0");
                                animationController.reverse();
                                setState(() {
                                  screenTitle = "Placement Phase";
                                });
                              },
                            ),
                          ),
                        ),
                        Transform(
                          transform: Matrix4.rotationZ(
                              getRadiansFromDegree(rotationAnimation.value)),
                          alignment: Alignment.center,
                          child: CircularButton(
                            color: Colors.red,
                            width: 60,
                            height: 60,
                            icon: Icon(
                              Icons.menu,
                              color: Colors.white,
                            ),
                            onClick: () {
                              if (animationController.isCompleted) {
                                animationController.reverse();
                              } else {
                                animationController.forward();
                              }
                            },
                          ),
                        )
                      ],
                    ))
              ],
            ),
          ),
          drawer: Theme(
            data: Theme.of(context)
                .copyWith(canvasColor: Color.fromRGBO(236, 239, 255, 1.0)),
            child: Drawer(
              child: ListView(
                children: [
                  Container(
                    child: DrawerHeader(
                      child: Text(
                        "Select Unit to Place",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 40),
                      ),
                    ),
                    //color: Colors.lightBlueAccent,
                  ),
                  ExpansionTile(
                    initiallyExpanded: true,
                    title: Text(
                      armyOne,
                      textAlign: TextAlign.center,
                    ),
                    children: [
                      armyOneUnits == null
                          ? Container()
                          : Container(
                              child: ListView.builder(
                                itemCount: armyOneUnits.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return armyOneUnits.length == 0
                                      ? Text(
                                          "Looks like you haven't added any units to this army!")
                                      : new ListTile(
                                          title:
                                              armyOneCustomModelNames[index] == "" ? Text("${armyOneUnits[index]['name']} ${armyOneWeapons[index][0]["name"]}") : Text(armyOneCustomModelNames[index]),
                                          onTap: () {
                                            assetName = armyOneCustomModelNames[index] == "" ?
                                                armyOneUnits[index]['name'] : armyOneCustomModelNames[index].substring(0, armyOneCustomModelNames[index].length-9);
                                            assetBundleUrl = armyOneCustomModelUrls[index] == "" ?
                                                bundleUrls[armyOneFaction] : armyOneCustomModelUrls[index];
                                            unit =
                                                "{no: '${armyOneUnits[index]["no"]}', a: '${armyOneUnits[index]["a"]}', sv: '${armyOneUnits[index]["sv"][0]}', m: '${armyOneUnits[index]["m"][0]}', bs: '${armyOneUnits[index]["bs"][0]}', s: '${armyOneUnits[index]["s"]}', t: '${armyOneUnits[index]["t"]}', imageUrl: '${armyOneUnits[index]["imageUrl"]}', w: '${armyOneUnits[index]["w"]}', ld: '${armyOneUnits[index]["ld"]}', name: '${armyOneUnits[index]["name"]}', sheetId: '${armyOneUnits[index]["sheetId"]}', id: '${armyOneUnits[index]["id"]}', ws: '${armyOneUnits[index]["ws"][0]}'}";
                                            //unit = jsonEncode(armyOneUnits[index].toString());
                                            if (armyOneWeapons[index].length ==
                                                2) {
                                              weapon =
                                                  "[{wID: '${armyOneWeapons[index][0]["wID"]}', str: '${armyOneWeapons[index][0]["str"]}', d: '${armyOneWeapons[index][0]["d"]}', name: '${armyOneWeapons[index][0]["name"]}', range: '${armyOneWeapons[index][0]["range"]}', id: '${armyOneWeapons[index][0]["id"]}', type: '${armyOneWeapons[index][0]["type"]}', filterType: '${armyOneWeapons[index][0]["filterType"]}', ap: '${armyOneWeapons[index][0]["ap"]}'} > {wID: '${armyOneWeapons[index][1]["wID"]}', str: '${armyOneWeapons[index][1]["str"]}', d: '${armyOneWeapons[index][1]["d"]}', name: '${armyOneWeapons[index][1]["name"]}', range: '${armyOneWeapons[index][1]["range"]}', id: '${armyOneWeapons[index][1]["id"]}', type: '${armyOneWeapons[index][1]["type"]}', filterType: '${armyOneWeapons[index][1]["filterType"]}', ap: '${armyOneWeapons[index][1]["ap"]}'}]";
                                              //weapon = jsonEncode(armyOneWeapons[index].toString());
                                            } else {
                                              weapon =
                                                  "[{wID: '${armyOneWeapons[index][0]["wID"]}', str: '${armyOneWeapons[index][0]["str"]}', d: '${armyOneWeapons[index][0]["d"]}', name: '${armyOneWeapons[index][0]["name"]}', range: '${armyOneWeapons[index][0]["range"]}', id: '${armyOneWeapons[index][0]["id"]}', type: '${armyOneWeapons[index][0]["type"]}', filterType: '${armyOneWeapons[index][0]["filterType"]}', ap: '${armyOneWeapons[index][0]["ap"]}'} > ]";
                                              //weapon = jsonEncode(armyOneWeapons[index].toString());
                                            }
                                            spawnModel(
                                                _unityWidgetController,
                                                assetName,
                                                assetBundleUrl,
                                                unit,
                                                weapon);
                                            _scaffoldKey.currentState
                                                .openEndDrawer();
                                          },
                                        );
                                },
                              ),
                              height: 250,
                            )
                    ],
                  ),
                  ExpansionTile(
                    initiallyExpanded: true,
                    title: Text(
                      armyTwo,
                      textAlign: TextAlign.center,
                    ),
                    children: [
                      armyTwoUnits == null
                          ? Container()
                          : Container(
                              child: ListView.builder(
                                itemCount: armyTwoUnits.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return armyTwoUnits.length == 0
                                      ? Text(
                                          "Looks like you haven't added any units to this army!")
                                      : new ListTile(
                                          title:
                                          armyTwoCustomModelNames[index] == "" ? Text("${armyTwoUnits[index]['name']} ${armyTwoWeapons[index][0]["name"]}") : Text(armyTwoCustomModelNames[index]),
                                          onTap: () {
                                            assetName =
                                            armyTwoCustomModelNames[index] == "" ? armyTwoUnits[index]['name'] : armyTwoCustomModelNames[index].substring(0, armyTwoCustomModelNames[index].length-9);
                                            assetBundleUrl = armyTwoCustomModelUrls[index] == "" ?
                                            bundleUrls[armyTwoFaction] : armyTwoCustomModelUrls[index];
                                            print("Army two custom Url ${armyTwoCustomModelUrls[index]}");
                                            unit =
                                                "{no: '${armyTwoUnits[index]["no"]}', a: '${armyTwoUnits[index]["a"]}', sv: '${armyTwoUnits[index]["sv"][0]}', m: '${armyTwoUnits[index]["m"][0]}', bs: '${armyTwoUnits[index]["bs"][0]}', s: '${armyTwoUnits[index]["s"]}', t: '${armyTwoUnits[index]["t"]}', imageUrl: '${armyTwoUnits[index]["imageUrl"]}', w: '${armyTwoUnits[index]["w"]}', ld: '${armyTwoUnits[index]["ld"]}', name: '${armyTwoUnits[index]["name"]}', sheetId: '${armyTwoUnits[index]["sheetId"]}', id: '${armyTwoUnits[index]["id"]}', ws: '${armyTwoUnits[index]["ws"][0]}'}";
                                            //unit = jsonEncode(armyTwoUnits[index].toString());
                                            print(
                                                "unit: $unit --------------------------------------------------------------------------------------------------------------------------");
                                            if (armyTwoWeapons[index].length ==
                                                2) {
                                              weapon =
                                                  "[{wID: '${armyTwoWeapons[index][0]["wID"]}', str: '${armyTwoWeapons[index][0]["str"]}', d: '${armyTwoWeapons[index][0]["d"]}', name: '${armyTwoWeapons[index][0]["name"]}', range: '${armyTwoWeapons[index][0]["range"]}', id: '${armyTwoWeapons[index][0]["id"]}', type: '${armyTwoWeapons[index][0]["type"]}', filterType: '${armyTwoWeapons[index][0]["filterType"]}', ap: '${armyTwoWeapons[index][0]["ap"]}'} > {wID: '${armyTwoWeapons[index][1]["wID"]}', str: '${armyTwoWeapons[index][1]["str"]}', d: '${armyTwoWeapons[index][1]["d"]}', name: '${armyTwoWeapons[index][1]["name"]}', range: '${armyTwoWeapons[index][1]["range"]}', id: '${armyTwoWeapons[index][1]["id"]}', type: '${armyTwoWeapons[index][1]["type"]}', filterType: '${armyTwoWeapons[index][1]["filterType"]}', ap: '${armyTwoWeapons[index][1]["ap"]}'}]";
                                              //weapon = jsonEncode(armyTwoWeapons[index].toString());
                                            } else {
                                              weapon =
                                                  "[{wID: '${armyTwoWeapons[index][0]["wID"]}', str: '${armyTwoWeapons[index][0]["str"]}', d: '${armyTwoWeapons[index][0]["d"]}', name: '${armyTwoWeapons[index][0]["name"]}', range: '${armyTwoWeapons[index][0]["range"]}', id: '${armyTwoWeapons[index][0]["id"]}', type: '${armyTwoWeapons[index][0]["type"]}', filterType: '${armyTwoWeapons[index][0]["filterType"]}', ap: '${armyTwoWeapons[index][0]["ap"]}'} > ]";
                                              //weapon = jsonEncode(armyTwoWeapons[index].toString());
                                            }
                                            spawnModel(
                                                _unityWidgetController,
                                                assetName,
                                                assetBundleUrl,
                                                unit,
                                                weapon);
                                            _scaffoldKey.currentState
                                                .openEndDrawer();
                                          },
                                        );
                                },
                              ),
                              height: 250,
                            )
                    ],
                  ),
                ],
              ),
            ),
          ),
          backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
        ),
      ),
    );
  }

  /*
  unit 1: {
    name: "Veteran"
    A: "3"
   */

  // Communication from Unity to Flutter
  void onUnityMessage(message) {
    print('Received message from unity: ${message.toString()}');
    String unityMessageOut = message.toString();
    if (unityMessageOut[0] == "1") {
      _setPhase("3");
      Fluttertoast.showToast(
          msg: unityMessageOut.substring(1),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else if (unityMessageOut[0] == "2") {
      _setPhase("2");
      Fluttertoast.showToast(
          msg: unityMessageOut.substring(1),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    } else if (unityMessageOut[0] == "3") {
      _setPhase("2");
      showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) => AlertDialog(
            title: const Text('Attack Sequence'),
            content: Text(unityMessageOut.substring(1)),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.pop(context, 'Continue'),
                child: Text("Continue"),
              ),
            ]),
      );
    } else {
      print("What did I just receive?? $unityMessageOut");
    }
  }

  // Callback that connects the created controller to the unity controller
  void onUnityCreated(UnityWidgetController controller) async {
    this._unityWidgetController = controller;
/*    this
        ._unityWidgetController
        .postMessage("Scripts", 'LoadModel', '$assetCat');*/
    final _isPaused = await this._unityWidgetController.isPaused();
    //final _isUnloaded = await this._unityWidgetController.isLoaded();
    if (_isPaused) {
      Future.delayed(
        Duration(milliseconds: 50),
        () async {
          await this._unityWidgetController.resume();
        },
      );
    }
/*    if(_isUnloaded != true){
      Future.delayed(
        Duration(milliseconds: 50),
            () async {
          await this._unityWidgetController.resume();
        },
      );
    }*/
  }

  // Communication from Unity when new scene is loaded to Flutter
  void onUnitySceneLoaded(SceneLoaded sceneInfo) {
    print('Received scene loaded from unity: ${sceneInfo.name}');
    print(
        'Received scene loaded from unity buildIndex: ${sceneInfo.buildIndex}');
    //this._unityWidgetController.postMessage("Scripts", 'LoadModel', '$modelUrl');
  }

  void onUnityUnloaded() {
    this._unityWidgetController.unload();
  }

  void quitUnity(UnityWidgetController controller) async {
    this._unityWidgetController = controller;
    this._unityWidgetController.postMessage("Scripts", "DeleteAllUnits", "");
  }

  void spawnModel(UnityWidgetController controller2, String modelName,
      String modelUrl, String modelDetails, String weaponDetails) async {
    this._unityWidgetController = controller2;
    assetCat = "$modelUrl|$modelName|$modelDetails|$weaponDetails";
    this
        ._unityWidgetController
        .postMessage("Scripts", 'LoadModel', "$assetCat");
  }

  void _setPhase(String phase) async {
    print("Setting Phase: " + phase);
    _unityWidgetController.postMessage(
        'Scripts',
        //'LoadModelScript',
        'SetPhase',
        //objectUrl
        phase);
  }
}

class CircularButton extends StatelessWidget {
  final double width;
  final double height;
  final Color color;
  final Icon icon;
  final Function onClick;

  CircularButton(
      {this.color, this.width, this.height, this.icon, this.onClick});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: color, shape: BoxShape.circle),
      width: width,
      height: height,
      child: IconButton(icon: icon, enableFeedback: true, onPressed: onClick),
    );
  }
}

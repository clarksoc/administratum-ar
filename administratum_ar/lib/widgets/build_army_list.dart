import 'package:administratum_ar/screens/army_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

Widget buildArmyList(
    BuildContext context, DocumentSnapshot documentSnapshot, int index) {
  return documentSnapshot["name"] == "unassigned"
      ? Container()
      : Container(
          child: TextButton(
            child: Container(
              padding: const EdgeInsets.fromLTRB(0, 15, 0, 15),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Text(
                          "Army Name",
                          style: TextStyle(
                              color: Color.fromRGBO(63, 72, 129, 1.0),
                              fontSize: 12),
                        ),
                        Text(
                          "${documentSnapshot["name"]}",
                          style: TextStyle(
                              color: Color.fromRGBO(4, 9, 43, 1.0), fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        Text(
                          "Army Faction",
                          style: TextStyle(
                              color: Color.fromRGBO(63, 72, 129, 1.0),
                              fontSize: 12),
                        ),
                        Text(
                          "${documentSnapshot["faction"]}",
                          style: TextStyle(
                              color: Color.fromRGBO(4, 9, 43, 1.0), fontSize: 15),
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ArmyScreen(armyName: documentSnapshot["name"])));
            },
          ),
    decoration: BoxDecoration(
      border: Border.all(color: Colors.black38),
    ),
        );
}

import 'package:administratum_ar/models/database_func_army.dart';
import 'package:administratum_ar/models/database_func_models.dart';
import 'package:administratum_ar/screens/models_uploaded.dart';
import 'package:administratum_ar/screens/profile_screen.dart';
import 'package:administratum_ar/screens/unity_battle_screen.dart';
import 'package:administratum_ar/screens/upload_screen.dart';
import 'package:administratum_ar/widgets/build_army_list.dart';
import 'package:administratum_ar/widgets/loading.dart';
import 'package:administratum_ar/widgets/open_dialog.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_screen.dart';

class MainMenuScreen extends StatefulWidget {
  @override
  _MainMenuScreenState createState() => _MainMenuScreenState();
}

class _MainMenuScreenState extends State<MainMenuScreen>
    with WidgetsBindingObserver {
  var fireInstance = FirebaseFirestore.instance;
  var fireAuth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  SharedPreferences sharedPreferences;
  bool isLoading = false;
  String userId;
  String displayName = "";
  TextEditingController _textFieldController = TextEditingController();
  String valueText = "";
  String armyName = "";
  String dropFaction = 'Select Faction';
  List<String> factionList = ['Select Faction'];
  List<String> armyList = ['Select Army'];
  String armyOne = "Army 1";
  String armyTwo = "Army 2";
  var listArray = [];


  Future<String> getData() async {
    factionList = ['Select Faction'];
    List<String> tempFact = await makeFactionNameList();
    armyList = await getArmyList(userId);
    setState(() {
      factionList += tempFact;
    });
    return "Success";
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    readLocal();
    getData();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void readLocal() async {
    sharedPreferences = await SharedPreferences.getInstance();
    userId = sharedPreferences.getString("id") ?? "";
    displayName = sharedPreferences.getString("displayName") ?? "";

    setState(() {});
  }

  List<Selection> selections = const <Selection>[
    //Drop down menu options
    const Selection(title: "Profile", iconData: Icons.person),
    const Selection(title: "Log out", iconData: Icons.exit_to_app),
  ];

  onMenuPress(selection) {
    if (selection.title == "Log out") signOutHandler();
    if (selection.title == "Profile")
      Navigator.push(
          context, MaterialPageRoute(builder: (cxt) => ProfileScreen()));
    return;
  }

  Future<Null> signOutHandler() async {
    this.setState(() {
      isLoading = true;
    });
    await fireAuth.signOut();
    await googleSignIn
        .disconnect(); //needed for when user logged in with google
    await googleSignIn.signOut();

    this.setState(() {
      isLoading = false;
    });

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (ctx) =>
                LoginScreen(
                  title: "Administratum AR",
                )),
            (Route<dynamic> route) => false);
  }

  Future<bool> onBackPress() {
    openDialog(context);
    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Administratum AR"),
        backgroundColor: Color.fromRGBO(22, 30, 84, 1.0),
        actions: <Widget>[
          PopupMenuButton<Selection>(
            onSelected: onMenuPress,
            itemBuilder: (BuildContext context) {
              return selections.map((Selection selection) {
                return PopupMenuItem<Selection>(
                  value: selection,
                  child: Row(
                    children: <Widget>[
                      Icon(selection.iconData),
                      Container(
                        width: 10.0,
                      ),
                      Text(selection.title),
                    ],
                  ),
                );
              }).toList();
            },
          )
        ],
      ),
      drawer: Theme(
        data: Theme.of(context)
            .copyWith(canvasColor: Color.fromRGBO(236, 239, 255, 1.0)),
        child: Drawer(
          child: ListView(
            children: [
              Container(
                child: DrawerHeader(
                  child: Text(
                    "Administratum AR",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 30),
                  ),
                ),
                alignment: Alignment.bottomCenter,
              ),
              ExpansionTile(
                title: Text(
                  "BATTLE!",
                  textAlign: TextAlign.center,
                ),
                onExpansionChanged: (value) {
                  setState(() {
                    getData();
                  });
                },
                children: [
                  Text(
                    "Select Two Armies for Battle!",
                    textAlign: TextAlign.center,
                  ),
                  ExpansionTile(
                    title: Text(
                      "$armyOne",
                      textAlign: TextAlign.center,
                    ),
                    children: [
                      Container(
                        child: ListView.builder(itemCount: armyList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return armyList[index] == "unassigned"
                                  ? Container()
                                  : new ListTile(
                                title: Text(armyList[index]), onTap: (){
                                  setState(() {
                                    armyOne = armyList[index];
                                  });
                              },
                              );
                            }
                        ),
                        height: 200,
                      ),
                    ],
                  ),
                  ExpansionTile(
                    title: Text(
                      "$armyTwo",
                      textAlign: TextAlign.center,
                    ),
                    children: [
                      Container(
                        child: ListView.builder(itemCount: armyList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return armyList[index] == "unassigned"
                                  ? Container()
                                  : new ListTile(
                                title: Text(armyList[index]), onTap: (){
                                setState(() {
                                  getData();
                                  armyTwo = armyList[index];
                                });
                              },
                              );
                            }
                        ),
                        height: 200,
                      ),
                    ],
                  ),
                  ListTile(
                    title: Text("To Battle!",
                      textAlign: TextAlign.center,
                    ),
                    onTap: (){
                      trySubmit();
                    },
                  ),
                ],
              ),
/*              ListTile(
                title: Text(
                  "BATTLE!",
                  textAlign: TextAlign.center,
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ArmySelectionScreen()));
                },
              ),*/
              ListTile(
                title: Text(
                  "Upload Custom Model",
                  textAlign: TextAlign.center,
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UploadModelScreen()));
                },
              ),
              ListTile(
                title: Text(
                  "Uploaded Models",
                  textAlign: TextAlign.center,
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ModelsUploaded(armyName: '',)));
                },
              ),
            ],
          ),
        ),
      ),
      body: WillPopScope(
        child: Stack(
          children: [
            Container(
              child: StreamBuilder(
                stream: fireInstance
                    .collection("users")
                    .doc(userId)
                    .collection("ListArmies")
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Loading();
                  } else {
                    return snapshot.data.docs.length == 1
                        ? Center(
                      child: Text("No Created Armies Yet!"),
                    )
                        : ListView.builder(
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (context, index) {
                        final army =
                            snapshot.data.docs[index].reference.id;
                        return Dismissible(
                          key: Key(army),
                          onDismissed: (direction) async {
                            await deleteArmy(
                                snapshot.data.docs[index]["name"]);
                            setState(() {
                              army.remove();
                            });
                          },
                          background: Container(
                            color: Colors.red,
                            alignment: Alignment.centerRight,
                            child: Container(
                              padding: EdgeInsets.fromLTRB(
                                  0.0, 0.0, 15.0, 0.0),
                              child: Icon(
                                Icons.delete_forever,
                                size: 40.0,
                              ),
                            ),
                          ),
                          child: buildArmyList(
                              context, snapshot.data.docs[index], index),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        ),
        onWillPop: onBackPress,
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          addArmy(context);
        },
        backgroundColor: Color.fromRGBO(255, 81, 81, 1.0),
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      backgroundColor: Color.fromRGBO(236, 239, 255, 1.0),
    );
  }

  void trySubmit(){
    if (armyOne == "Army 1" || armyTwo == "Army 2"){
      Fluttertoast.showToast(
        msg: "Please Select Two Armies to Battle!",
        backgroundColor: Colors.grey,
        textColor: Colors.black,
      );
    }else{
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UnityBattleScreen(armyOne: armyOne, armyTwo: armyTwo,)));
    }
  }

  Future<void> deleteArmy(String delArmyName) async {
    await fireInstance
        .collection("users")
        .doc(userId)
        .collection(delArmyName)
        .get()
        .then((snapshot) {
      for (DocumentSnapshot doc in snapshot.docs) {
        doc.reference.delete();
      }
    });
    await fireInstance
        .collection('users')
        .doc(userId)
        .collection('ListArmies')
        .where('name', isEqualTo: delArmyName)
        .get()
        .then((snapshot) {
      for (DocumentSnapshot doc in snapshot.docs) {
        doc.reference.delete();
      }
    });
    setState(() {
      getData();
    });
  }

  Future<void> addArmy(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Text('Add New Army'),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    onChanged: (value) {
                      valueText = value;
                      setState(() {});
                    },
                    controller: _textFieldController,
                    decoration: InputDecoration(hintText: "New Army Name"),
                  ),
                  DropdownButton(
                    value: dropFaction,
                    items: factionList
                        .map<DropdownMenuItem<String>>((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList() ??
                        [],
                    onChanged: (newVal) {
                      dropFaction = newVal;
                      setState(() {});
                    },
                  ),
                ],
              ),
              actions: <Widget>[
                TextButton(
                  child: Text('CANCEL'),
                  onPressed: () {
                    dropFaction = 'Select Faction';
                    _textFieldController.clear();
                    setState(() {
                      getData();
                    });
                    Navigator.pop(context);
                  },
                ),
                TextButton(
                  child: Text('ADD'),
                  onPressed:
                  ((dropFaction == 'Select Faction') || (valueText.trim() == ""))
                      ? null
                      : () {
                    addNewArmy(
                      userId,
                      valueText,
                      dropFaction,
                    );
                    _textFieldController.clear();
                    dropFaction = 'Select Faction';
                    setState(() {
                      getData();
                    });
                    Navigator.pop(context);
                  },
                ),
              ],

            );
          });
        });
  }
}

class Selection {
  final String title;
  final IconData iconData;

  const Selection({this.title, this.iconData});
}
